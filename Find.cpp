#include <algorithm>
#include <array>
#include <cassert>
#include <iostream>
#include <map>
#include <regex>
#include <set>
#include <string>
#include <type_traits>
#include <unordered_map>

template<class T>
struct is_pair : std::false_type
{};

template<class K, class V>
struct is_pair<std::pair<K, V>> : std::true_type
{};

int main()
{
	std::map<std::string, int> m {{"abc", 1}, {"cde", 2}, {"efg", 3}};

	int n1 = 3;

	auto lambda = [&n1](auto it) { return it.second == n1; };
	auto result1 = std::find_if(m.begin(), m.end(), lambda);

	(result1 != m.end()) ? std::cout << "map contains " << n1 << '\n' : std::cout << "map does not contain " << n1 << '\n';

	std::unordered_map<std::string, std::string> u = {{"red", "#FF0000"}, {"green", "#00FF00"}, {"blue", "#0000FF"}};

	auto lambda2 = [&n1](auto it) { return std::regex_match(it.second, std::regex("(#)(.*)")); };

	int result2 = std::count_if(u.begin(), u.end(), lambda2);

	std::cout << "unordered map contains " << result2 << " hex colors" << '\n';

	std::set<int> c {2, 3, 4, 5435345, 2};

	auto lambda3 = [](int const& i) { std::cout << i << '\t'; };

	std::for_each(c.begin(), c.end(), lambda3);
}
